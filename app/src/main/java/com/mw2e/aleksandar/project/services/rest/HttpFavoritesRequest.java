package com.mw2e.aleksandar.project.services.rest;

import android.os.AsyncTask;
import com.mw2e.aleksandar.project.MainActivity;
import com.mw2e.aleksandar.project.models.Recipe;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Aleksandar on 1/23/2017.
 */
public class HttpFavoritesRequest  extends AsyncTask<String, Void, List<Recipe>> {

    @Override
    protected  List<Recipe> doInBackground(String... params) {

        String email = null;
        String token = null;

        String url = params[0] + "/what2eat/get_favorites";
        email = params[1];
        token = params[2];

        RestTemplate rest = new RestTemplate();

        HttpMessageConverter formHttpMessageConverter = new FormHttpMessageConverter();
        HttpMessageConverter stringHttpMessageConverternew = new StringHttpMessageConverter();
        rest.getMessageConverters().add(formHttpMessageConverter);
        rest.getMessageConverters().add(stringHttpMessageConverternew);
        rest.getMessageConverters().add(new GsonHttpMessageConverter());

        HttpHeaders requestHeaders = new HttpHeaders();
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("email", email);
        map.add("token", token);

        HttpEntity requestEntity = new HttpEntity(map, requestHeaders);
        Recipe[] responseEntity = rest.postForObject(url, map, Recipe[].class);

        return Arrays.asList(responseEntity);
    }
}