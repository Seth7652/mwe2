package com.mw2e.aleksandar.project;

import android.view.View;
import com.mw2e.aleksandar.project.models.Recipe;

/**
 * Created by Aleksandar on 1/18/2017.
 */
public interface MyClickListener {
    public void itemClicked(View view, Recipe r);
}
