package com.mw2e.aleksandar.project.fragments;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.mw2e.aleksandar.project.MainActivity;
import com.mw2e.aleksandar.project.R;
import com.mw2e.aleksandar.project.models.Recipe;
import com.mw2e.aleksandar.project.services.rest.HttpAuthenticationRequest;
import com.mw2e.aleksandar.project.services.rest.HttpRequestRecipe;
import java.util.concurrent.ExecutionException;

public class SingleRecipeFragment extends Fragment {

    private static final String ARG_PARAM1 = "recipe_id";
    private  String url = "/what2eat/recipe/";

    private HttpAuthenticationRequest task2;
    private String recipe_id;
    private HttpRequestRecipe task;
    private ArrayAdapter<String> adapter;
    private ListView listView;
    private TextView textView;
    private WebView webView;
    private FloatingActionButton btn;

    public SingleRecipeFragment(){  task = new HttpRequestRecipe();

    }

    public static SingleRecipeFragment newInstance(String recipe_id){
        SingleRecipeFragment fragment = new SingleRecipeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, recipe_id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        MainActivity m = (MainActivity) getActivity();
        url = m.getBase_url() + url;
        if (getArguments() != null) {
            recipe_id = getArguments().getString(ARG_PARAM1);
        }
        task.execute(url + recipe_id);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.fragment_single_recipe, container, false);

        Recipe r = null;
        try {
           r = task.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        textView = (TextView) rootView.findViewById(R.id.recipe_title_text_view);
        textView.setText(r.getTitle());
        /*adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_checked, r.getIngredients());
        listView = (ListView) rootView.findViewById(R.id.ingredients_list_view);
        listView.setChoiceMode(listView.CHOICE_MODE_NONE);
        listView.setAdapter(adapter);*/
        final String source_url = r.source_url;
        webView = (WebView) rootView.findViewById(R.id.webview);
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading (WebView view, WebResourceRequest request) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    view.loadUrl(request.getUrl().toString());
                }
                else view.loadUrl(source_url);
                return true;
            }
        });
        webView.loadUrl(source_url);
        btn = (FloatingActionButton) rootView.findViewById(R.id.floating_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                task2 = new HttpAuthenticationRequest();
                MainActivity activity = (MainActivity) getActivity();
                String email = activity.getUserEmail();
                String token = activity.getToken();
                if(email == null) {
                    Toast.makeText(activity, "you are not signed in", Toast.LENGTH_LONG).show();
                }else
                task2.execute("http://192.168.178.44:8080/what2eat/add_favorites/" + recipe_id, email, token);
                Toast.makeText(getActivity(), "added to favorites", Toast.LENGTH_LONG).show();
            }
        });

        return rootView;
    }
}
