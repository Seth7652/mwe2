package com.mw2e.aleksandar.project.services.rest;

import android.os.AsyncTask;
import com.mw2e.aleksandar.project.models.Recipe;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Aleksandar on 1/14/2017.
 */
public class HttpRequestRecipe extends AsyncTask<String, Void, Recipe> {

    @Override
    protected Recipe doInBackground(String... params) {
        String url = params[0];
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new GsonHttpMessageConverter());

        HttpHeaders requestHeaders = new HttpHeaders();
        /*if (params[1] != null) requestHeaders.add("Cookie", params[1]);*/
        HttpEntity requestEntity = new HttpEntity(null, requestHeaders);
        ResponseEntity<Recipe> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Recipe.class);

        return responseEntity.getBody();
    }
}