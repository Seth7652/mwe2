package com.mw2e.aleksandar.project;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.mw2e.aleksandar.project.fragments.MainActivityFragment;
import com.mw2e.aleksandar.project.fragments.RecipesFragment;
import com.mw2e.aleksandar.project.services.rest.HttpAuthenticationRequest;

public class MainActivity extends AppCompatActivity {

    public String base_url = "http://192.168.178.44:8080";
    private  String url = base_url + "/what2eat/register";
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private boolean authenticated;
    private HttpAuthenticationRequest task;
    private String userEmail;
    private String token;


    public MainActivity(){
        this.task = new HttpAuthenticationRequest();
    }

    public String getUserEmail(){
        return userEmail;
    }

    public String getToken(){
        return token;
    }

    public String getBase_url(){
        return base_url;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        init();

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser mUser = mAuth.getCurrentUser();
        if(mAuth.getCurrentUser() != null){
            userEmail = mUser.getEmail();
            authenticated = true;
            mUser.getToken(true)
                    .addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                        public void onComplete(@NonNull Task<GetTokenResult> task) {
                            if(task.isSuccessful()) {
                                token = task.getResult().getToken();
                                sendToken(token);
                            } else{
                                // Handle error -> task.getException();
                            }
                        }
                    });
        } else{
            authenticated = false;
        }
        mAuthListener = new FirebaseAuth.AuthStateListener(){
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if(firebaseAuth.getCurrentUser() == null){
                    Toast.makeText(MainActivity.this, "logging out", Toast.LENGTH_LONG).show();
                    authenticated = false;
                }
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
            if (id == R.id.log_out) {
                if(authenticated) {
                    mAuth.signOut();
                }else Toast.makeText(MainActivity.this, "you are not signed in", Toast.LENGTH_LONG).show();
                return true;
            }
            if(id == R.id.sign_in){
                if(authenticated){
                    Toast.makeText(MainActivity.this, "you are already signed in", Toast.LENGTH_LONG).show();
                }else
                    startActivity(new Intent(MainActivity.this, GoogleSignInActivity.class));
                return true;
            }
            if(id == R.id.favorites){
                RecipesFragment f = new RecipesFragment();
                swapFragment(f);
                return true;
            }
            if(id == R.id.home){
                if(authenticated) {
                    init();
                }else Toast.makeText(MainActivity.this, "you are not signed in", Toast.LENGTH_LONG).show();
                return true;
            }

        return super.onOptionsItemSelected(item);
    }

    public void init(){
        MainActivityFragment f = new MainActivityFragment();
        swapFragment(f);
    }

    public void swapFragment(Fragment f){
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.parent_container,f);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    public void sendToken(String token){
        task.execute(url,userEmail,token);
    }
}
