package com.mw2e.aleksandar.project.models;



import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by Aleksandar on 1/9/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Recipe{

    public String publisher;
    public String f2f_url;
    public List<String> ingredients;
    public String title;
    public String source_url;
    public String recipe_id;
    public String image_url;
    public double social_rank;
    public String publisher_url;

    public Recipe() {}

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getF2f_url() {
        return f2f_url;
    }

    public void setF2f_url(String f2f_url) {
        this.f2f_url = f2f_url;
    }

    public List<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSource_url() {
        return source_url;
    }

    public void setSource_url(String source_url) {
        this.source_url = source_url;
    }

    public String getRecipe_id() {
        return recipe_id;
    }

    public void setRecipe_id(String recipe_id) {
        this.recipe_id = recipe_id;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public double getSocial_rank() {
        return social_rank;
    }

    public void setSocial_rank(double social_rank) {
        this.social_rank = social_rank;
    }

    public String getPublisher_url() {
        return publisher_url;
    }

    public void setPublisher_url(String publisher_url) {
        this.publisher_url = publisher_url;
    }

    @Override
    public String toString(){
        return String.format("%s", this.title);
    }

    public String getSerarchQuery(){
        String s = "";
        for (int i = 0; i < ingredients.size(); i++) {
            s += ingredients.get(i);
            if(i != ingredients.size() - 1) s += "%2C+";
        }
        return s;
    }
}