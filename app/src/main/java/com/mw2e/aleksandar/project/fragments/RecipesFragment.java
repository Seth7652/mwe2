package com.mw2e.aleksandar.project.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.mw2e.aleksandar.project.MainActivity;
import com.mw2e.aleksandar.project.MyClickListener;
import com.mw2e.aleksandar.project.R;
import com.mw2e.aleksandar.project.adapters.RecipeAdapter;
import com.mw2e.aleksandar.project.models.Recipe;
import com.mw2e.aleksandar.project.services.rest.HttpFavoritesRequest;
import com.mw2e.aleksandar.project.services.rest.HttpRequestResults;
import java.util.concurrent.ExecutionException;

public class RecipesFragment extends Fragment implements MyClickListener{

    private static final String ARG_PARAM1 = "recipe_id";

    private RecyclerView mRecyclerView;
    private RecipeAdapter mAdapter;
    private HttpRequestResults task;
    private HttpFavoritesRequest task2;
    private String url = "/what2eat/results/";

    private String query;

    public RecipesFragment() {
        task2 = new HttpFavoritesRequest();
        task = new HttpRequestResults();

    }

    public static RecipesFragment newInstance(String query) {
        RecipesFragment fragment = new RecipesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, query);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainActivity mainActivity = (MainActivity) getActivity();
        String email = mainActivity.getUserEmail();
        String token = mainActivity.getToken();
        String base_url = mainActivity.getBase_url();

        if (getArguments() != null) {query = getArguments().getString(ARG_PARAM1);
            task.execute(base_url + url + query);
        }
        else{
            if(email != null)   task2.execute(base_url, email, token);
            else    Toast.makeText(mainActivity, "you are not signed in", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_recipes, container, false);
        TextView textView = (TextView) rootView.findViewById(R.id.results_title_text_view);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recipes_recycler);
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new RecipeAdapter(getActivity());
        try {
            if(query != null) {
                mAdapter.setMDataset(task.get());
            }
            else{
                mAdapter.setMDataset(task2.get());
                textView.setText("Favorites");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        mAdapter.setMyOnClickListener(this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return rootView;
    }

    @Override
    public void itemClicked(View view, Recipe r) {
        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.swapFragment(SingleRecipeFragment.newInstance(r.getRecipe_id()));
    }
}
