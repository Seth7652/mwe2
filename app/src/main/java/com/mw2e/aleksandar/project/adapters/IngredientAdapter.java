package com.mw2e.aleksandar.project.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.mw2e.aleksandar.project.R;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Aleksandar on 1/9/2017.
 */
public class IngredientAdapter extends RecyclerView.Adapter<IngredientAdapter.IngredientViewHolder> {

    private List<String> mDataset;
    private LayoutInflater layoutInflater;
    private ViewGroup parent;
    private Context context;
    private Set<String> helper;

    public IngredientAdapter(Context context){
        this.context = context;
        mDataset = new ArrayList<String>();
        helper = new TreeSet<String>(mDataset);
    }

    public void addItem(String ingredient){
        if(helper.add(ingredient))  this.mDataset.add(ingredient);
    }

    @Override
    public IngredientAdapter.IngredientViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        layoutInflater = LayoutInflater.from(parent.getContext());
        this.parent = parent;
        View v = layoutInflater
                .inflate(R.layout.ingredient_view, this.parent, false);

        RecyclerView.ViewHolder vh = new IngredientViewHolder(v);
        return (IngredientViewHolder) vh;
    }

    @Override
    public void onBindViewHolder(IngredientAdapter.IngredientViewHolder holder, int position) {
        String ingredientName = this.mDataset.get(position);
        holder.ingredintTextView.setText(ingredientName);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class IngredientViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public View mView;
        public TextView ingredintTextView;

        public IngredientViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            itemView.setOnClickListener(this);
            ingredintTextView = (TextView) itemView.findViewById(R.id.ingredient_text_view);
        }

        @Override
        public void onClick(View view) {
        }
    }

    public String getSerarchQuery(){
        String s = "";
        for (int i = 0; i < mDataset.size(); i++) {
            s += mDataset.get(i);
            if(i != mDataset.size() - 1) s += "%2C+";
        }
        return s;
    }
}