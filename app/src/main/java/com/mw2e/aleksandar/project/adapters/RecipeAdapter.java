package com.mw2e.aleksandar.project.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.mw2e.aleksandar.project.MyClickListener;
import com.mw2e.aleksandar.project.R;
import com.mw2e.aleksandar.project.models.Recipe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aleksandar on 1/15/2017.
 */
public class RecipeAdapter extends RecyclerView.Adapter<RecipeAdapter.RecipeViewHolder> {

    private List<Recipe> mDataset;
    private LayoutInflater layoutInflater;
    private ViewGroup parent;
    private Context context;
    private MyClickListener myClickListener;

    public RecipeAdapter(Context context){
        this.context = context;
        mDataset = new ArrayList<Recipe>();
    }

    public RecipeAdapter(List<Recipe> mDataset, Context context){
        this.context = context;
        this.mDataset = mDataset;
    }

    public void setMyOnClickListener(MyClickListener myOnClickListener){
        this.myClickListener = myOnClickListener;
    }

    public void addItem(Recipe r){
        this.mDataset.add(r);
    }

    public void setMDataset(List<Recipe> recipes){
        mDataset = recipes;
    }

    @Override
    public RecipeAdapter.RecipeViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        layoutInflater = LayoutInflater.from(parent.getContext());
        this.parent = parent;
        View v = layoutInflater
                .inflate(R.layout.recipe_item_view, this.parent, false);

        RecyclerView.ViewHolder vh = new RecipeAdapter.RecipeViewHolder(v);
        return (RecipeAdapter.RecipeViewHolder) vh;
    }

    @Override
    public void onBindViewHolder(RecipeAdapter.RecipeViewHolder holder, int position) {
        Recipe r = this.mDataset.get(position);
        holder.recipeTextView.setText(r.toString());
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class RecipeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public View mView;
        public TextView recipeTextView;

        public RecipeViewHolder(View itemView){
            super(itemView);
            mView = itemView;
            itemView.setOnClickListener(this);
            recipeTextView = (TextView) itemView.findViewById(R.id.recipe_text_view);
        }

        @Override
        public void onClick(View view) {
            if(myClickListener != null){
                myClickListener.itemClicked(view, mDataset.get(getAdapterPosition()));
            }
        }
    }
}