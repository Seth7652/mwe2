package com.mw2e.aleksandar.project.services.rest;
import android.os.AsyncTask;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Aleksandar on 1/22/2017.
 */
public class HttpAuthenticationRequest  extends AsyncTask<String, Void, Void> {

    @Override
    protected Void doInBackground(String... params) {
        String url = params[0];
        String email = params[1];
        String token = params[2];

        RestTemplate rest = new RestTemplate();

        HttpMessageConverter formHttpMessageConverter = new FormHttpMessageConverter();
        HttpMessageConverter stringHttpMessageConverternew = new StringHttpMessageConverter();
        rest.getMessageConverters().add(formHttpMessageConverter);
        rest.getMessageConverters().add(stringHttpMessageConverternew);

        HttpHeaders requestHeaders = new HttpHeaders();
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("email", email);
        map.add("token", token);

        HttpEntity requestEntity = new HttpEntity(map, requestHeaders);

        String result = rest.postForObject(url, map, String.class);
        System.out.println(result);

        return null;
    }
}
