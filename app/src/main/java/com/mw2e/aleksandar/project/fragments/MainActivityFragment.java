package com.mw2e.aleksandar.project.fragments;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import com.mw2e.aleksandar.project.MainActivity;
import com.mw2e.aleksandar.project.R;
import com.mw2e.aleksandar.project.adapters.IngredientAdapter;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment{

    private RecyclerView mRecyclerView;
    private IngredientAdapter mAdapter;
    private EditText editText;

    public MainActivityFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        editText = (EditText) rootView.findViewById(R.id.input_ingredient);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.ingredient_recycler);
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new IngredientAdapter(getActivity());
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        Button b1 = (Button) rootView.findViewById(R.id.button_add);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String text = editText.getText().toString();
                if(!text.trim().equals("")) {
                    mAdapter.addItem(text);
                    mAdapter.notifyDataSetChanged();
                    editText.getText().clear();
                }
            }
        });
        Button b2 = (Button) rootView.findViewById(R.id.button_results);
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity mainActivity = (MainActivity) getActivity();
                if(mAdapter.getItemCount() == 0){
                    RecipesFragment f = RecipesFragment.newInstance("all");
                    mainActivity.swapFragment(f);
                }
                else {
                    RecipesFragment f = RecipesFragment.newInstance(mAdapter.getSerarchQuery());
                    mainActivity.swapFragment(f);
                }
            }
        });

        return rootView;
    }
}
